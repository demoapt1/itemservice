import {NextFunction, Request, Response} from "express";
import { ItemData } from "../data/ItemData";

export class ItemController {

    private itemData = new ItemData();

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            let items = await this.itemData.all(request.params);
            response.json({status: 0, items: items});
        } catch (err) {
            console.log(err);
            response.json({status: 1, error: 'failed to fetch items'});
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
            let item = await this.itemData.one(request.params);
            response.json({status: 0, items: item});
        } catch(err) {
            console.log(err);
            response.json({status: 1, error: 'failed to fetch item'});
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
            await this.itemData.save(request.body);
            let item = await this.itemData.one(request.body);
            response.json({status: 0, items: item});
        } catch(err) {
            console.log(err);
            response.json({status: 1, error: 'failed to save item'});
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            await this.itemData.remove(request.body);
            response.json({ status: 0, message: 'the item has been removed' });
        } catch (err) {
            console.log(err);
            response.json({ status: 1, message: 'failed to remove the item' });
        }
    }

}