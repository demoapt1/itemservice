import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class ItemHistory {

    @PrimaryGeneratedColumn()
    ItemHistoryID: number;

    @Column()
    ItemID: string;

    @Column()
    UserID: string;

    @Column()
    Quantity: number;

    @Column()
    Action: string;

    @Column()
    DateOfAction: Date;

}
